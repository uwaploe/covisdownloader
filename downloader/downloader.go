// Package downloads files using SFTP and removes them from the remote system.
package downloader

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"os/user"
	"strings"

	"github.com/pkg/sftp"
)

const Usage = `Usage: covisdownload [options] sftp://HOST/PATH datadir

Download COVIS data from an SFTP server.
`

type appEnv struct {
	srcDir    string
	dataDir   string
	linkDir   string
	server    string
	user      string
	keyFile   string
	keepFiles bool
	fl        *flag.FlagSet
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("downloader", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	fl.StringVar(&app.keyFile, "key", "", "SSH private key file")
	fl.BoolVar(&app.keepFiles, "keep", false, "If true, keep srcdir files")
	fl.StringVar(&app.linkDir, "link", "", "Directory to link data files into")

	app.fl = fl
	if err := fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) < 2 {
		return errors.New("Missing command-line arguments")
	}

	u, err := url.Parse(req[0])
	if err != nil {
		return fmt.Errorf("URL parse error %q: %w", req[0], err)
	}

	if app.user = u.User.Username(); app.user == "" {
		user, err := user.Current()
		if err != nil {
			return fmt.Errorf("Cannot lookup user name: %w", err)
		}
		app.user = user.Username
	}

	if strings.Index(u.Host, ":") != -1 {
		app.server = u.Host
	} else {
		app.server = u.Host + ":22"
	}
	app.srcDir = u.Path[1:]
	app.dataDir = req[1]

	return nil
}

func (app *appEnv) run(ctx context.Context) error {
	conn, err := pubkeyConnect(app.user, app.server, app.keyFile)
	if err != nil {
		return err
	}
	defer conn.Close()

	c, err := sftp.NewClient(conn)
	if err != nil {
		return err
	}
	defer c.Close()

	n, err := downloadDirFiles(ctx, c, app.srcDir, app.dataDir, app.linkDir, app.keepFiles)
	log.Printf("Downloaded %d files", n)

	return err
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}
	if err = app.run(ctx); err != nil {
		fmt.Fprintf(os.Stderr, "Runtime error: %v\n", err)
		return 1
	}
	return 0
}
