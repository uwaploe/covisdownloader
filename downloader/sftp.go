package downloader

import (
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/pkg/sftp"
)

func downloadFile(c *sftp.Client, srcPath, dstDir, linkDir string) (int64, error) {
	fin, err := c.Open(srcPath)
	if err != nil {
		return 0, err
	}
	defer fin.Close()

	filename := filepath.Base(srcPath)
	dstPath := filepath.Join(dstDir, filename)
	fout, err := os.Create(dstPath)
	if err != nil {
		return 0, err
	}
	defer fout.Close()

	n, err := io.Copy(fout, fin)
	fout.Close()
	if linkDir != "" {
		os.Link(dstPath, filepath.Join(linkDir, filename))
	}

	return n, err
}

const TIMEFMT = "20060102T150405Z"

// Create storage directory based on the data file timestamp. The timestamp
// is incorporated into the filename:
//
//    COVIS-YYYYmmddTHHMMSS.tar.gz -> YYYY/jjj/COVIS-...
//
// Where "jjj' is the year-day.
//
func genDirName(filename string) (string, error) {
	ts, err := time.Parse(TIMEFMT, filename[6:21]+"Z")
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%04d/%03d", ts.Year(), ts.YearDay()), nil
}

func makeDstDir(dataDir, dataFile string) (string, error) {
	d, err := genDirName(dataFile)
	if err != nil {
		return d, err
	}
	dirname := filepath.Join(dataDir, d)
	err = os.MkdirAll(dirname, 0755)
	return dirname, err
}

func downloadDirFiles(ctx context.Context, c *sftp.Client,
	srcDir, dataDir, linkDir string, keep bool) (int, error) {
	var count int

	listing, err := c.ReadDir(srcDir)
	if err != nil {
		return count, err
	}

	for _, fi := range listing {
		select {
		case <-ctx.Done():
			return count, ctx.Err()
		default:
		}

		if fi.IsDir() || !strings.HasPrefix(fi.Name(), "COVIS-") {
			continue
		}

		path := c.Join(srcDir, fi.Name())
		dstDir, err := makeDstDir(dataDir, fi.Name())
		if err != nil {
			return count, fmt.Errorf("Cannot create destination directory for %q: %w",
				fi.Name(), err)
		}

		_, err = downloadFile(c, path, dstDir, linkDir)
		if err != nil {
			return count, fmt.Errorf("Download failed %q: %w", path, err)
		}
		count++

		if !keep {
			err = c.Remove(path)
			if err != nil {
				return count, fmt.Errorf("Cannot remove %q: %w", path, err)
			}
		}
	}

	return count, nil
}
