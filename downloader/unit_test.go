package downloader

import (
	"flag"
	"reflect"
	"testing"
)

func TestCmdLine(t *testing.T) {
	table := []struct {
		in  []string
		out appEnv
		err error
	}{
		{
			in:  []string{"--version"},
			out: appEnv{},
			err: versReq,
		},
		{
			in:  []string{"-help"},
			out: appEnv{},
			err: flag.ErrHelp,
		},
		{
			in: []string{"--key", "keyfilename", "sftp://user@host/some/path", "datadir"},
			out: appEnv{srcDir: "some/path", dataDir: "datadir", user: "user",
				server: "host:22", keyFile: "keyfilename"},
		},
		{
			in: []string{"sftp://user@host:2022//some/path", "datadir"},
			out: appEnv{srcDir: "/some/path", dataDir: "datadir", user: "user",
				server: "host:2022"},
		},
	}

	for _, e := range table {
		var app appEnv
		err := app.fromArgs(e.in)
		if err != e.err {
			t.Fatal(err)
		}
		app.fl = nil
		if !reflect.DeepEqual(app, e.out) {
			t.Errorf("Bad value; expected %#v, got %#v",
				e.out, app)
		}

	}
}

func TestDirName(t *testing.T) {
	table := []struct {
		in, out string
	}{
		{
			in:  "COVIS-20201116T003002-diffuse1.tar.gz",
			out: "2020/321",
		},
		{
			in:  "COVIS-20200101T163002-diffuse1.tar.gz",
			out: "2020/001",
		},
	}

	for _, e := range table {
		d, err := genDirName(e.in)
		if err != nil {
			t.Error(err)
		}
		if e.out != d {
			t.Errorf("Bad value; expected %q, got %q",
				e.out, d)
		}
	}
}
